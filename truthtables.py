#!/usr/bin/python
# -*- coding: utf8 -*-
# --shell-escape
import sys,string

#def tfSeq(size):# production des tables pour les variables
  #if size > 1:        
    #for head in tfSeq(size-1):
      #yield head + [True];yield head + [False]
  #else:
    #yield [True];yield [False]

def tfSeq(size):# production des tables pour les variables
  global desc
  if size > 1:        
    for head in tfSeq(size-1):
      yield head + [desc];yield head + [1-desc]
  else:
    yield [desc];yield [1-desc]

def oper(b): # arithmétique des opérateurs (pour avoir le xor et la barre de shaeffer)
  if type(b) == type(tuple()):
    if len(b)==2:#cas de la négation
      return "(1-"+oper(b[1])+")"
    elif len(b)==3:
      p=oper(b[0])
      q=oper(b[2])
      if b[1]=='>':
        return "(1-"+p+"+"+p+"*"+q+")"
      elif b[1]=='v':
        return "("+p+"+"+q+"-"+p+"*"+q+")"
      elif b[1]=='&':
        return "("+p+"*"+q+")"
      elif b[1]=='=':
        return "(1-"+p+"-"+q+"+(2*"+p+"*"+q+"))"
      elif b[1]=="+":
        return "("+p+"+"+q+"-2*"+p+"*"+q+")"
      elif b[1]=="|":
          return "(1-"+p+"*"+q+")"
  else:
    return str(b)

def get_variables(a):# repère les variables et les rend dans l'ordre alphabetique dans un tuple
  l= list(string.ascii_lowercase.replace('v',''))+list(string.ascii_uppercase)# toutes les litteraux minuscules ou majuscules - le v est interdit, il est utilise pour le ou
  s=''
  for i in a:
    if (i in l) and not (i in s):
      s+=i
  return str(list(''.join(sorted(s)))).replace("'",'').replace(' ','')[1:-1]

def analysys(i):
  if len(i)==1:
    return str(i)
  dans=0
  n=0
  while True:
    if i[n]=='(':
      dans+=1
    else:
      if i[n]==')':
        dans-=1
    if (dans==0) and (i[n] in ['>','v','&','=','+','|']):
      break
    n+=1
    if n==len(i):
      break
  if n==len(i):#sortie sans trouver d'operateur binaire
    if i[0]=='-':
      return ('-',analysys(i[1:]))
    else:
      return analysys(i[1:-1])# on enleve les parentheses
  else:
    return (analysys(i[:n]),i[n],analysys(i[n+1:]))
  
def sonde(i,prof=0): # meme struture mais retourne la profondeur des sous-formules
  if len(i)==1:
    return (prof)
  dans=0
  n=0
  while True:
    if i[n]=='(':
      dans+=1
    else:
      if i[n]==')':
        dans-=1
    if (dans==0) and (i[n] in ['>','v','&','=','+','|']):
      break
    n+=1
    if n==len(i):
      break
  if n==len(i):#sortie sans trouver d'operateur
    if i[0]=='-':
      return (prof,sonde(i[1:],prof+1))
    else:
      return sonde(i[1:-1],prof+1)# on enleve les parentheses
  else:
    return (sonde(i[:n],prof+1),prof,sonde(i[n+1:],prof+1))
  
def aplatit(f,par=True):# aplatit un tuple - utilisé pour les formules ou les couleurss
  x=''
  for i in f:
    if type(i) is type(tuple()):
      if par==True:
        x+='('+aplatit(i,par)+')'
      else:
        x+=aplatit(i,par)
    else:
      if type(i)==int and i>9: # pour les couleurs, si 9 on repasse à 0
        i=i-9
      x+=str(i)
  return x

def elim(f):
  return str(f).replace("'","").replace(',','').replace(' ','')

colonne=0# numero de la colonne

def extract(b,vs,a=0,p=0,prof=0):
  if type(b) == type(tuple()):
    if len(b)==2:#cas de la negation
      #return "b.append(('"+"("*(a)+"-' ,lambda "+vs+": "+"-("+oper(b[1])+") ))\n"+str(extract(b[1],vs,0,p,prof+1))
      return "b.append(('"+"("*(a)+"-' ,lambda "+vs+": "+oper(b)+" ))\n"+str(extract(b[1],vs,0,p,prof+1))
    else:
      if len(b)==3:
        return str(extract(b[0],vs,a+1,0))+"b.append((' "+str(b[1])+"',lambda "+vs+": "+oper(b)+" ))\n"+str(extract(b[2],vs,0,p+1))
  else:
    return "b.append(('"+"("*a+str(b)+")"*p+"',lambda "+vs+": "+str(b)+" ))\n"
    
color=''
lg=[]

def detaille(f):
  global variables,color
  y=sonde(f)
  if type(y)!=int: 
    y=aplatit(y,False)
  else:
    y=str(y)
  color+=y# on ajoute les couleurs de cette formule
  lg.append(len(y))# recupere le nombre de colonnes de la formule
  f=extract(analysys(f),variables)
  return f

f=sys.argv[1].replace(' ','');# fonctions sans espace
variables=get_variables(f)
vars=variables.split(',')
f=f.split(',')# function dans un tuple

#chargement des options ou des valeur par defaut
if len(sys.argv)==2:
  vv=('1','0')
  ecriture=('neg ','land ','lor ','equiv ','supset ','oplus ','mid ')
  cadre=False;detail=False
  desc=1
  coloration=1
else:
  vv=sys.argv[2].split(',')
  ecriture=sys.argv[3].split(',')
  cadre=(sys.argv[4].strip()=='1')
  detail=(sys.argv[5].strip()=='1')
  desc=sys.argv[6].strip()=='1'
  coloration=sys.argv[7].strip()=='1'

b=[]
for i in f:
  if detail:
    exec(detaille(i))
  else:
    exec("b.append(('"+i+"',lambda "+variables+":"+oper(analysys(i))+" ))\n")
    lg.append(1) # incremente longueur
    
def truthtable(vars, funcs):
    global ecriture,color,lg,detail,coloration
    output = ''
    lVars = str(len(vars))
    color='0'*int(lVars)+color
    for i in range(int(lVars)):
      lg=[1]+lg
    lFuncs = str(len(funcs))
    labels = vars +[func[0] for func in funcs]
    align=''
    c=0
    for i in labels:#repérage des alignements des colonnes
      if i[0]=='(':
        align+='r'
      elif i[len(i)-1]==')':
        align+='l'
      else:
        align+='c'
      c+=1
    outputlocal=''
    c=0
    for i in lg:
      for j in range(0,i):
        if detail and coloration:
          outputlocal+=r'>{\color{truthtc'+color[c]+'}}'+align[c]
        else:
          if detail:
            outputlocal+=align[c]
          else:
            outputlocal+='c'
        c+=1
      outputlocal+='|'
    if cadre:
      output += '\n\\hline\n'
    #labels = vars +[func[0] for func in funcs]
    output += '£'.join(['$'+label+'$' for label in labels]) + r'\\'# £ au lieu de & pour traiter tousles remplacements d'un coup (conflit avec le & de la conjonction évité)
    conorig=("-","&","v","=",">",'+','|')
    i=0
    while i<7:
      output=output.replace(conorig[i],"\\"+ecriture[i]+" ")
      i+=1
    output=output.replace('£','&')# rétablissement du séparateur de colonne
    output=r'\addtolength{\tabcolsep}{-3pt}\begin{tabular}{|'+outputlocal+'}'+output
    output += '\n\\hline\n'
    #tfText = {True:vv[0], False:vv[1]}
    tfText = {1:vv[0],0:vv[1]}
    for tf in tfSeq(len(vars)):# parcours des mondes possibles
      values = [tfText[val] for val in tf] 
      values += ([tfText[func[1](*tf)] for func in funcs])
      output += '&'.join(values) + '\\\\\n'
      output += '\n'
      if cadre:
        output +='\\hline\n'  
    output += r'\end{tabular}\addtolength{\tabcolsep}{3pt}'
    return output
   
print(truthtable(vars,b))

sys.stderr.close(); sys.stdout.close()


    




    
    
