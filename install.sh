#!/bin/bash
pdflatex --shell-escape --interaction=nonstopmode truthtables.dtx
pdflatex truthtables.ins
a=`whereis texlive`/texmf-dist/tex/latex/truthtables
mkdir $a
cp truthtables.pdf $a
cp truthtables.sty $a
cp truthtables.py $a
sudo texhash
pdflatex --shell-escape --interaction=nonstopmode test
evince test.pdf

